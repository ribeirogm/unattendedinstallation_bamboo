# Automating Bamboo Setup Wizard
Why fill in web setup wizards when you can automate them?

# Requirements

You'll need Bamboo installed but not setup.
You'll also need Python and the Mechanize package installed.

To install Mechanize using Pip:

```
pip install mechanize
```

# Files

For express intallation (database: H2 user:root password:root)

```
python install_express.py
```
